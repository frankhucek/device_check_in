use std::env;
use std::io;
use std::io::Read;
use std::io::Write;
use std::net;
use std::thread;
use std::time::Duration;
use chrono::prelude::*;
use std::net::{
    TcpListener,
    TcpStream,
    Ipv4Addr, 
    SocketAddrV4
};
use rand;

use device_check_in::server;

fn server() -> io::Result<()> {
    let listener = net::TcpListener::bind("0.0.0.0:5500")?;
    println!("listening on 5500 for tcp conn");
    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                println!("connection to new client");
                handle_tcp_stream(stream);
            }
            Err(e) => {
                println!("failed to make connection {:?}", e);
            }
        }
    }
    Ok(())
}

fn start_test_server(mut server_connection: TcpStream) -> bool {
    let start_time = Utc::now().timestamp();
    let end_time = start_time + 30;

    let listener = TcpListener::bind("0.0.0.0:1313").unwrap();
    listener.set_nonblocking(true).expect("Cannot set non-blocking");

    let rn: u16 = rand::random();

    let req_thread = thread::spawn(move || {
        let server_test_string = format!("? {}:SERVER TEST ME ?", rn.to_string());
        server_connection.write(server_test_string.as_bytes())
            .expect("Failed to write to server_connection");
    });

    for stream in listener.incoming() {
        match stream {
            Ok(mut s) => {
                println!("got a connection!");
                
                s.set_read_timeout(Some(Duration::from_secs(10)))
                    .expect("Could not set read timeout of server connection");
                let mut server_data = String::new();
                s.read_to_string(&mut server_data)
                    .expect("No data from connected server");

                if  server_data.contains(&rn.to_string()) {
                    return true
                } else if Utc::now().timestamp() >= end_time {
                    return false
                } else {
                    continue;
                }
            }
            Err(ref e) if e.kind() == io::ErrorKind::WouldBlock => {
                thread::sleep(Duration::from_secs(1));
                if Utc::now().timestamp() >= end_time {
                    return false
                } else {
                    continue;
                }
            }
            Err(e) => panic!("encountered IO error: {}", e),
        }
    }
    req_thread.join()
        .expect("Failed to join request thread");
    true
}

fn client() -> io::Result<()> {
    for i in 1..20 {
        let mut stream = net::TcpStream::connect("127.0.0.1:5500")?;
        println!("connected");
        let str = format!("{} hello tcp server", i);
        stream.write(str.as_bytes())?;
        thread::sleep(Duration::from_secs(1));
    }
    println!("exiting");
    Ok(())
}

fn handle_tcp_stream(mut stream: net::TcpStream) {
    let mut buffer = String::new();
    match stream.read_to_string(&mut buffer) {
        Ok(_) => {
            println!("received: {}", buffer);
        }
        Err(e) => println!("Error reading from socket {:?}", e),
    }
}

fn init_server_test() {
    
}

fn main() {
    let is_server = server::Server::init_server_test();
    println!("{}", is_server);
}
