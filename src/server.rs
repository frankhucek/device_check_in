use std::env;
use std::io;
use std::io::Error;
use std::io::ErrorKind;
use std::io::Read;
use std::io::Write;
use std::net;
use std::thread;
use std::time::Duration;
use chrono::prelude::*;
use std::net::{
    TcpListener,
    TcpStream,
    Ipv4Addr, 
    SocketAddrV4
};
use rand;

pub struct Server {
    trusted_servers: Vec<String>,
    
}

// 'static' impl block
impl Server {
    pub fn new(mut servers: Vec<String>) -> Server {
        let mut default_server = vec![String::from("ripplein.space:1313")]; // default servers eventually come from somewhere else
        servers.append(&mut default_server);

        Server {
            trusted_servers: servers
        }
    }

    pub fn new_no_servers() -> Server {
        let mut servers = vec![String::from("ripplein.space:1313")]; // default servers eventually come from somewhere else

        Server {
            trusted_servers: servers
        }
    }

    pub fn init_server_test() -> bool {
        let server = Server::new_no_servers(); 

        for trusted_peer in server.trusted_servers.iter() {
            println!("trying {}", trusted_peer);
            // make tcp connection, 
            match ping_server(trusted_peer) {
                Ok(stream) => {
                    return start_test_server(stream)
                }
                Err(e) => continue
            }
        }

        false
    }
}

fn start_test_server(mut server_connection: TcpStream) -> bool {
    let start_time = Utc::now().timestamp();
    let end_time = start_time + 30;

    let listener = TcpListener::bind("0.0.0.0:1313").unwrap();
    listener.set_nonblocking(true).expect("Cannot set non-blocking");

    let rn: u16 = rand::random();

    let req_thread = thread::spawn(move || {
        let server_test_string = format!("? {}:SERVER TEST ME ?", rn.to_string());
        server_connection.write(server_test_string.as_bytes())
            .expect("Failed to write to server_connection");
    });

    for stream in listener.incoming() {
        match stream {
            Ok(mut s) => {
                println!("got a connection!");
                
                s.set_read_timeout(Some(Duration::from_secs(10)))
                    .expect("Could not set read timeout of server connection");
                let mut server_data = String::new();
                s.read_to_string(&mut server_data)
                    .expect("No data from connected server");

                if  server_data.contains(&rn.to_string()) {
                    return true
                } else if Utc::now().timestamp() >= end_time {
                    return false
                } else {
                    continue;
                }
            }
            Err(ref e) if e.kind() == io::ErrorKind::WouldBlock => {
                thread::sleep(Duration::from_secs(1));
                if Utc::now().timestamp() >= end_time {
                    return false
                } else {
                    continue;
                }
            }
            Err(e) => panic!("encountered IO error: {}", e),
        }
    }
    req_thread.join()
        .expect("Failed to join request thread");
    true
}

// should eventually be client::ping_server
fn ping_server(addr: &str) -> Result<TcpStream, Error> {

    match TcpStream::connect(addr) {
        Ok(mut stream) => {
            stream.set_read_timeout(Some(Duration::from_secs(10)))?;

            let rn: u16 = rand::random();
            let server_ping_msg = format!("? {}:SERVER PING ?", rn.to_string());

            stream.write(server_ping_msg.as_bytes())?;
            println!("SENDING PING TO SERVER: {}", server_ping_msg);
            let mut server_resp = String::new();
            stream.read_to_string(&mut server_resp)?;
            println!("RECEIVED PING RESP FROM SERVER: {}", server_resp);

            let expected_resp = format!("? {} ?", rn.to_string()); // eventual regex all this
            if server_resp.trim() == expected_resp {
                println!("server responded correctly to ping");
                return Ok(stream)
            } else {
                println!("Server failed verification");
                Err(Error::new(ErrorKind::InvalidData, "Failed to respond correctly"))
            }
        }
        Err(e) => {
            println!("Failed to even ping this server. We should remove this from trusted_peers");
            return Err(e)
        }
    }

}

// 'instance' impl block
impl Server {
    
}