use std::net::{
    TcpListener,
    TcpStream,
    Ipv4Addr, 
    SocketAddrV4
};
use std::thread;
use std::str::FromStr;
use chrono::prelude::*;

#[derive(Debug)]
pub struct DeviceNode {
    pub trusted_peers: Vec<String>,
    pub created_at: String,
}

impl DeviceNode {

    pub fn new(peers: &[&str]) -> DeviceNode {

        let mut trusted_peers: Vec<String> = vec![];

        for peer in peers.iter() {
            trusted_peers.push(peer.to_string());
        }



        let node = DeviceNode {
            trusted_peers: trusted_peers,
            created_at: Utc::now().to_string(),
        };



        node
    }

    fn initial_setup(&self, ) -> Result<(), &str> {
        let res = ("127.0.0.1", 1313);
        // ping a trusted peer
        Ok(())
    }

    pub fn ping_peer(&self) -> Result<(), &str> { // send hostname, created_at
        for peer in self.trusted_peers.iter() {
            match TcpStream::connect(peer) {
                Ok(mut stream) => {
                    self.sync_with_server(stream);
                    return Ok(())
                }
                Err(e) => {}
            }
        }
        Ok(())
    }

    fn sync_with_server(&self, stream: TcpStream) {

    }

    pub fn run(&self) {

    }

    fn run_server(&self) {

    }

    fn run_client(&self) {

    }
}